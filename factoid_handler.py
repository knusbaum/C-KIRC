#!/usr/bin/env python2

import sys
import json
import re
import datetime
import os.path

def load_factoids():
    if not os.path.isfile("factoid.txt"):
        write_factoids({"facts": {}, "admins": [], "adders": []})

    with open("factoid.txt", "r") as factoids:
        return json.loads(factoids.read())

def write_factoids(f):
    with open("factoid.txt", "w") as factoids:
        factoids.write(json.dumps(f, indent=4))

def respond(message, response):
    if m["command"] == "PRIVMSG":
        if m["params"][0].startswith("#"):
            return "PRIVMSG " + m["params"][0] + " :" + response + "\r\n"
        else:
            return "PRIVMSG " + m["prefix"]["server_nick"] + " :" + response + "\r\n"
    return ""

def is_adder(message):
    return m["prefix"]["server_nick"] in factoid["adders"] + factoid["admins"]

def is_admin(message):
    return m["prefix"]["server_nick"] in factoid["admins"]

factoid = load_factoids()

while True:
    sys.stdout.flush()
    line = sys.stdin.readline()

    try:
        m = json.loads(line)
        if m["command"] == "PRIVMSG":

            fact_match = re.search('fact +([^ ]*)?|,([^ ]*)$', m["trailing"])
            if fact_match:
                factname = fact_match.group(1) or fact_match.group(2)
                if factname and factname in factoid["facts"].keys():
                    print respond(m, factoid["facts"][factname]["fact"])
                    continue

            factinfo_match = re.search('factinfo +([^ ]+)$', m["trailing"])
            if factinfo_match:
                factname = factinfo_match.group(1)
                if factname in factoid["facts"].keys():
                    print respond(m, "Fact: " + factname + " created by " + factoid["facts"][factname]["adder"] + " on " + factoid["facts"][factname]["date"])
                else:
                    print respond(m, "Fact: " + factname + " does not exist.")
                continue

            add_match = re.search('factadd( +([^ ]*) +(.*))?$', m["trailing"])
            if add_match:
                if not is_adder(m):
                    print respond(m, "You don't have permission for that.\n")
                    continue

                factname = add_match.group(2)
                fact = add_match.group(3)
                factoid["facts"][factname] = {"fact": fact, "adder": m["prefix"]["server_nick"], "date": str(datetime.datetime.now())}
                write_factoids(factoid)
                print respond(m, "Added fact: " + factname)
                continue

            factdel_match = re.search('factdel +([^ ]+)$', m["trailing"])
            if factdel_match:
                if not is_adder(m):
                    print respond(m, "You don't have permission for that.\n")
                    continue

                factname = factdel_match.group(1)
                if factname in factoid["facts"].keys():
                    del factoid["facts"][factname]
                    write_factoids(factoid)
                    print respond(m, "Removed fact: " + factname)
                else:
                    print respond(m, "Fact: " + factname + " does not exist.")
                continue

            useradd_match = re.search('useradd +([^ ]+)$', m["trailing"])
            if useradd_match:
                if not is_admin(m):
                    print respond(m, "You don't have permission for that.\n")
                    continue

                username = useradd_match.group(1)
                factoid["adders"].insert(0, username);
                write_factoids(factoid)
                print respond(m, "Added user " + username)
                continue

            userdel_match = re.search('userdel +([^ ]+)$', m["trailing"])
            if userdel_match:
                if not is_admin(m):
                    print respond(m, "You don't have permission for that.\n")
                    continue

                username = userdel_match.group(1)
                if not username in factoid["adders"]:
                    print respond(m, username + " is not a factoid user.")
                    continue
                factoid["adders"].remove(username);
                write_factoids(factoid)
                print respond(m, "Removed user " + username)
                continue

            factlist_match = re.search("factlist *$", m["trailing"])
            if factlist_match:
                print respond(m, json.dumps(factoid["facts"].keys()))
                continue

    except ValueError:
        with open("pyerror.txt", "a") as myfile:
            myfile.write("Failed to parse json: [" + line + "]\n")
        sys.exit(1)

GCC=cc
CFLAGS=-ggdb -O0 -std=gnu11 -I/usr/local/include
LFLAGS= -L/usr/local/lib -lpthread -lkgc
EXECUTABLE=kirc

CFILES=$(shell find src -name "*.c")
OBJECTS=$(patsubst %.c,%.o,$(CFILES))
DEPFILES=$(patsubst %.c,./deps/%.d,$(CFILES))


all : $(EXECUTABLE) client

-include $(DEPFILES)

$(EXECUTABLE) : $(OBJECTS)
	@echo -e "[LD]\t\t" $(EXECUTABLE)
	@$(GCC)  $(CFLAGS) $(OBJECTS) $(LFLAGS) -o $(EXECUTABLE)

client : src/connection.o src/configparser.o src/irchandler.o src/ircparser.o client.o
	@echo -e "[LD]\t\t" client
	@$(GCC)  $(CFLAGS) src/connection.o src/configparser.o src/irchandler.o src/ircparser.o client.o $(LFLAGS) -o client

src/%.o : src/%.c deps/src/%.d
	@echo -e "[CC]\t\t" $^
	@$(GCC) $(CFLAGS) -fpic -c $< -o $@

deps/%.d : %.c
	@mkdir -p deps
	@mkdir -p `dirname $@` 
	@echo -e "[MM]\t\t" $@
	@$(CC) $(CFLAGS) -MM $< -MF $@

clean :
	-@rm -R deps $(EXECUTABLE) client
	-@find . -name '*.o' -delete
	-@find . -name '*~' -delete

#ifndef IRCPARSER_H
#define IRCPARSER_H

struct prefix {
    char * server_nick;
    char * user;
    char * host;
};

struct param {
    char * parameter;
    struct param *next;
};

struct message {
    struct prefix prefix;
    char * command;
    struct param *params;
    char * trailing;
};

struct message *parse(int sock);
void print_message(struct message *m);
char * message_to_json(struct message *m);
char * message_to_IRC(struct message *m);

#endif

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <poll.h>
#include <unistd.h>
#include "connection.h"


#define DEBUG

#ifdef DEBUG
#define DEBUG_OUT(...) fprintf (stderr, __VA_ARGS__)
#else
#define DEBUG_OUT(...)
#endif


int launch_irc_socket(char *host, short port) {

    char s_port[6];
    sprintf(s_port, "%d", port);
    
    struct addrinfo hints;
    hints.ai_family = PF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = 0;
    hints.ai_flags = 0;

    socklen_t length;
    
    struct addrinfo *host_info;
    getaddrinfo(host, NULL, NULL, &host_info);
    if(host_info == NULL) {
        fprintf(stderr, "Failed to get address info for %s:%hd\n", host, port);
        exit(1);
    }
    ((struct sockaddr_in *)host_info->ai_addr)->sin_port = htons(port);
    
    DEBUG_OUT("\n");
    int type = host_info->ai_family;
    switch(type) {
        
    case AF_INET :
        DEBUG_OUT("Connecting on IPv4.\n");
        struct sockaddr_in addr;
        addr = *(struct sockaddr_in *)(host_info->ai_addr);
        DEBUG_OUT("Family: %d\n", addr.sin_family);
        DEBUG_OUT("Port: %d\n", ntohs(addr.sin_port));
        char *s_addr = inet_ntoa(addr.sin_addr);
        DEBUG_OUT("Address: %s\n", s_addr);
        length = sizeof (struct sockaddr_in);
        break;
        
    case AF_INET6 :
        DEBUG_OUT("Connecting on IPv6.\n");
        struct sockaddr_in6 addr6;
        addr6 = *(struct sockaddr_in6 *)(host_info->ai_addr);
        DEBUG_OUT("Family: %d\n", addr6.sin6_family);
        DEBUG_OUT("Port: %d\n", ntohs(addr6.sin6_port));
        DEBUG_OUT("Flow Info: %d\n", addr6.sin6_flowinfo);
        DEBUG_OUT("Address: ");
        for(int i = 0; i < 15; i++) {
            DEBUG_OUT("%X:", addr6.sin6_addr.s6_addr[i]);
        }
        DEBUG_OUT("%X\n", addr6.sin6_addr.s6_addr[15]);
        DEBUG_OUT("Scope ID: %d\n", addr6.sin6_scope_id);
        length = sizeof (struct sockaddr_in6);
        break;
        
    default:
        DEBUG_OUT("It's neither IPv4 or IPv6. Don't know if this will work.\n");
    }

    int sock = socket(type, SOCK_STREAM, 0);
    if(sock == -1) {
        perror("Failed to get socket");
        return -1;
    }
    
    int result = connect(sock, host_info->ai_addr, length);
    if(result != 0) {
        perror("Failed to connect");
        return -1;
    }

    freeaddrinfo(host_info);
    
    return sock;
}

void ensure_send(int sockfd, const char *buff, ssize_t len, int flags) {
    ssize_t count = 0;
    
    while(count < len) {
        count = send(sockfd, &buff[count], len-count, flags);
        
        if(count <= 0) {
            perror("Failed to send on socket");
            exit(1);
        }
    }
}

void quiet_ensure_send_str(int sockfd, const char *str, int flags) {
    size_t len = strlen(str);
    ensure_send(sockfd, str, len, flags);
}

void ensure_send_str(int sockfd, const char *str, int flags) {
    printf("<= %s\n", str);
    quiet_ensure_send_str(sockfd, str, flags);
}

void transfer(int fd_from, int fd_to) {
    while (1) {
        struct pollfd fds[1];
        
        fds[0].fd = fd_from;
        fds[0].events = POLLIN;
        int result = poll(fds, 1, 0);
        
        if(result > 0) {
            // Some readable data
            char buff[1024];
            int readcount = read(fd_from, buff, 1024);
            int wrcount = 0;
            while(wrcount < readcount) {
                int newwrcount = write(fd_to, buff, readcount);
                if(newwrcount < 0) {
                    perror("Failed to transfer data.\n");
                    exit(1);
                }
                wrcount += newwrcount;
            }
        }
        else if(result == 0) {
            // no readable data;
            return;
        }
        else {
            // poll failure
        }
    }
}

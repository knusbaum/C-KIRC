#ifndef IRCTOOLS_H
#define IRCTOOLS_H

#include "ircparser.h"

char *respond(struct message *m, char *response);
char *pong(struct message *m);

#endif

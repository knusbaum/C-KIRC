#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <ctype.h>
#include "ircparser.h"
#include <kgc.h>

static void do_prefix(int sock, struct message *m);
static void do_command(int sock, struct message *m);
static void do_params(int sock, struct message *m);
static void do_trailing(int sock, struct message *m);

char look;
char next;

static void get_char(int sock) {
    if(next) {
        look = next;
        next = 0;
        return;
    }
    
    ssize_t count = recv(sock, &look, 1, 0);
    if(count <= 0) {
        // Failed to read char.
        fprintf(stderr, "Failed to read from socket\n");
        exit(1);
    }

    if(look == '\"') {
        next = look;
        look = '\\';
    }
    if((look < 32 || look > 126)
       && look != '\n' && look != '\r'
       && look != '\t') {
        get_char(sock);
    }
}

static void match(int sock, char c) {
    if(look == c) {
        get_char(sock);
        return;
    }
    fprintf(stderr, "Wanted %c, got: %c", c, look);
    exit(1);
}

static void match_noget(char c) {
    if(look != c) {
        fprintf(stderr, "Wanted %c, got: %c", c, look);
        exit(1);
    }
}

struct message *parse(int sock) {
    struct message *m = gc_malloc(sizeof (struct message));

    if(!m) {
        fprintf(stderr, "Failed to allocate a message.\n");
        exit(1);
    }

    m->prefix.server_nick = NULL;
    m->prefix.user = NULL;
    m->prefix.host = NULL;
    m->command = NULL;
    m->params = NULL;
    m->trailing = NULL;

    if(look == 0) {
        get_char(sock);
    }

    if(look == ':') {
        do_prefix(sock, m);
    }

    do_command(sock, m);

    if(look != ':') {
        do_params(sock, m);
    }

    if(look == ':') {
        do_trailing(sock, m);
    }

    while(look != '\n') {
        get_char(sock);
    }
    look = 0;

    return m;
}


/***** Prefix *****/

static void do_user(int sock, struct message *m) {
    match(sock, '!');
    char buffer[1024];
    int index = 0;
    while(look != ' '
          && look != '@') {
        buffer[index++] = look;
        get_char(sock);
    }
    buffer[index] = 0;
    char *user = gc_malloc(strlen(buffer) + 1);
    strcpy(user, buffer);
    m->prefix.user = user;
}

static void do_host(int sock, struct message *m) {
    match(sock, '@');
    char buffer[1024];
    int index = 0;
    while(look != ' ') {
        buffer[index++] = look;
        get_char(sock);
    }
    buffer[index] = 0;
    char *host = gc_malloc(strlen(buffer) + 1);
    strcpy(host, buffer);
    m->prefix.host = host;
}

static void do_prefix(int sock, struct message *m) {
//    //printf("Matching colon.\n");
    match(sock, ':');
    char buffer[1024];
    int index = 0;
    while(look != ' ') {
        if(look == '!') {
            do_user(sock, m);
            continue;
        }
        if(look == '@') {
            do_host(sock, m);
            continue;
        }
        else {
            buffer[index++] = look;
        }
        get_char(sock);
    }
    buffer[index] = 0;
    char *server_nick = gc_malloc(strlen(buffer) + 1);
    strcpy(server_nick, buffer);
    m->prefix.server_nick = server_nick;
    match(sock, ' ');
}

/***** End Prefix *****/


/***** Command *****/

//static char *escape_command(char *c) {
//    char * quote = strchr(c, '\"');
//    if(quote) {
//        char * newbuff = gc_malloc(1024);
//        char * nbp = newbuff;
//        while(quote) {
//            memcpy(nbp, c, quote - c);
//            nbp[quote - c] = '\\';
//            nbp[quote - c + 1] = '\"';
//            nbp = &nbp[quote - c + 2];
//            c = quote + 1;
//            quote = strchr(c, '\"');
//        }
//        return newbuff;
//    }
//    return c;
//}

static void do_command(int sock, struct message *m) {
    char buffer[1024];
    int index = 0;
    while(look != ' ') {
        buffer[index++] = look;
        get_char(sock);
    }
    buffer[index] = 0;
    char * command = gc_malloc(strlen(buffer) + 1);
    strcpy(command, buffer);
    m->command = command;
}

/***** End Command *****/

/***** Params *****/

static void do_params(int sock, struct message *m) {
    while(look != ':'
          && look != '\r') {

        char buffer[1024];
        int index = 0;
        while(look != ' '
            && look != '\r') {
            buffer[index++] = look;
            get_char(sock);
        }
        if(look == ' ') {
            match(sock, ' ');
        }
        if(index == 0) continue;
        buffer[index] = 0;
        struct param *p = gc_malloc(sizeof (struct param));
        p->parameter = NULL;
        p->next = NULL;

        char * param = gc_malloc(strlen(buffer) + 1);
        strcpy(param, buffer);
        p->parameter = param;

        struct param *current = m->params;
        if(current) {
            while(current->next) {
                current = current->next;
            }
            current->next = p;
        }
        else {
            m->params = p;
        }
    }
}

/***** End Params *****/


/***** Trailing *****/

static void do_trailing(int sock, struct message *m) {
    match(sock, ':');

    char buffer[1024];
    int index = 0;
    while(look != '\r') {
        buffer[index++] = look;
        get_char(sock);
    }
    buffer[index] = 0;
    char * trailing = gc_malloc(strlen(buffer) + 1);
    strcpy(trailing, buffer);
    m->trailing = trailing;
}

/***** End Trailing *****/


void print_message(struct message *m) {
    printf("Message: \n");
    printf("\tPrefix:\n");
    printf("\t\tserver_nick: %s\n", m->prefix.server_nick);
    printf("\t\tuser: %s\n", m->prefix.user);
    printf("\t\thost: %s\n", m->prefix.host);
    printf("\tCommand:\n");
    printf("\t\tcommand: %s\n", m->command);

    struct param *p = m->params;
    while(p) {
        printf("\t\t%s\n", p->parameter);
        p = p->next;
    }

    printf("\tTrailing:\n");
    printf("\t\ttrailing: %s\n", m->trailing);
}

char * message_to_json(struct message *m) {

    char *json = gc_malloc(1024); //1024 should be plenty.
                                  //IRC max length is 512
                                  //and JSON is not very verbose.
    if(!json) { fprintf(stderr, "Failed to allocate buffer for JSON.\n"); }
    int index = 0;
    json[0] = 0;

    strcat(json, "{");

    if(m->prefix.server_nick) {
        strcat(json, "\"prefix\":{\"server_nick\":\"");
        strcat(json, m->prefix.server_nick);
        strcat(json, "\"");

        //only write out user if there is one.
        if(m->prefix.user) {
            strcat(json, ", \"user\":\"");
            strcat(json, m->prefix.user);
            strcat(json, "\"");
        }

        if(m->prefix.host) {
            strcat(json, ", \"host\":\"");
            strcat(json, m->prefix.host);
            strcat(json, "\"");
        }
        strcat(json, "},");
    }

    strcat(json, "\"command\":\"");
    strcat(json, m->command);
    strcat(json, "\"");

    if(m->params) {
        int first = 1;
        strcat(json, ", \"params\": [");
        struct param *p = m->params;
        while(p) {
            if(!first) {
                strcat(json, ",");
            }
            strcat(json, "\"");
            strcat(json, p->parameter);
            strcat(json, "\"");
            p = p->next;
            first = 0;
        }
        strcat(json, "]");
    }

    if(m->trailing) {
        strcat(json, ", \"trailing\": \"");
        strcat(json, m->trailing);
        strcat(json, "\"");
    }
    strcat(json, "}");
    return json;
}

char * message_to_IRC(struct message *m) {
    char *message = gc_malloc(1024);
    message[0] = ':';
    message[1] = 0;

    if(m->prefix.server_nick) {
        strcat(message, m->prefix.server_nick);

        if(m->prefix.host) {
            if(m->prefix.user) {
                strcat(message, "!");
                strcat(message, m->prefix.user);
            }
            strcat(message, "@");
            strcat(message, m->prefix.host);
        }
        strcat(message, " ");
    }

    strcat(message, m->command);

    struct param *p = m->params;
    while(p) {
        strcat(message, " ");
        strcat(message, p->parameter);
        p = p->next;
    }

    if(m->trailing) {
        strcat(message, " :");
        strcat(message, m->trailing);
    }

    strcat(message, "\r\n");

    return message;
}

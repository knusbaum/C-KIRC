#include "irctools.h"
#include "ircparser.h"
#include <kgc.h>
#include <string.h>
#include <stdio.h>

char *respond(struct message *m, char *response) {
    char *buff = gc_malloc(512);
    if(!buff) {
        fprintf(stderr, "FAILED TO ALLOC FOR RESPONSE!\n");
        exit(1);
    }

    sprintf(buff, "PRIVMSG %s :%s\r\n", m->params->parameter, response);
    return buff;
}

char *pong(struct message *m) {
    char *buff = gc_malloc(512);
    if(!buff) {
        fprintf(stderr, "FAILED TO ALLOC FOR PONG!\n");
        exit(1);
    }
    
    sprintf(buff, "PONG %s\r\n", m->trailing);
    return buff;
}

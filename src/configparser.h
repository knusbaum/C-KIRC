#ifndef CONFIGPARSER_H
#define CONFIGPARSER_H

#include "irchandler.h"

struct description {
    enum handler_type h_type;
    enum input_type i_type;
    char * command;
    struct description *next;
};

struct channel {
    char * channel_name;
    struct channel *next;
};

struct config {
    char * irc_host;
    char * local_host;
    short irc_port;
    short local_port;

    char * pass;

    char * nick;
    char * user;
    char * real_name;


    struct channel * channels;
    struct description *descriptions;
    struct description *last;
};


struct config *parse_config(char * config_file);


#endif

#ifndef CONNECTION_H
#define CONNECTION_H

/**
 * Make a connection to the IRC host at port.
 * This automatically prints errors from errno
 * if any occur.
 * return: a sockfd or -1 on error.
 */
int launch_irc_socket(char *host, short port);


/**
 * Like send, but buf must point to a string.
 * strlen is used to determine the length of the string.
 * This automatically prints errors from errno
 * if any occur. It will retry until there's no hope, and
 * then it will fail horribly (exits)
 */
void ensure_send_str(int sockfd, const char *str, int flags);
void quiet_ensure_send_str(int sockfd, const char *str, int flags);

void ensure_send(int sockfd, const char *buff, ssize_t len, int flags);

/**
 * Transfer all available data from one fd to another.
 */
void transfer(int fd_from, int fd_to);

#endif

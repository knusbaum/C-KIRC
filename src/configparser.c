#include "configparser.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <kgc.h>

int line;
int column;

char conf_look;

static void get_char(int fd) {
    ssize_t count = read(fd, &conf_look, 1);
    if(count <= 0) {
        // Failed to read char.
        fprintf(stderr, "Failed to read from socket\n");
        exit(1);
    }

    if(conf_look == '\n') {
        column = 0;
        line++;
    }
    else {
        column++;
    }
}

static void match(int fd, char c) {
    if(conf_look == c) {
        get_char(fd);
        return;
    }
    fprintf(stderr, "Line: %d Col: %d Wanted '%c', got: '%c'\n", line, column, c, conf_look);
    exit(1);
}

static void match_EOF(int fd, char c) {
    if(conf_look == c) {
        ssize_t count = read(fd, &conf_look, 1);
        if(count <= 0) {
            // Failed to read char.
            conf_look = EOF;
        }

        if(conf_look == '\n') {
            column = 0;
            line++;
        }
        else {
            column++;
        }
        return;
    }
    fprintf(stderr, "Line: %d Col: %d Wanted '%c', got: '%c'\n", line, column, c, conf_look);
    exit(1);
}

static void match_string(int fd, char *s) {

    while(s[0]) {
        if(conf_look != s[0]) {
            fprintf(stderr, "Line: %d Col: %d Wanted '%s'.\n", line, column, s);
            exit(1);
        }
        get_char(fd);
        s++;
    }
}

static void consume_spaces(int fd) {
    if(conf_look != ' '
       && conf_look != '\t') {
        fprintf(stderr, "Line: %d Col: %d Expected whitespace.\n", line, column);
    }
    while(conf_look == ' '
          || conf_look == '\t') {
        get_char(fd);
    }
}

static void do_htype(int fd, struct description *d) {
    switch(conf_look) {
    case 'P':
        match_string(fd, "PERSISTENT");
        d->h_type=PERSISTENT;
        break;
    case 'T':
        match_string(fd, "TRANSIENT");
        d->h_type = TRANSIENT;
        break;
    default:
        fprintf(stderr, "Line: %d Col: %d Expected (PERSISTENT | TRANSIENT)",
                line, column);
        exit(1);
        break;
    }
}

static void do_itype(int fd, struct description *d) {
    switch(conf_look) {
    case 'J':
        match_string(fd, "JSON");
        d->i_type = JSON;
        break;
    case 'R':
        match_string(fd, "RAW");
        d->i_type = RAW;
        break;
    default:
        fprintf(stderr, "Line: %d Col: %d Expected (JSON | RAW)",
                line, column);
        exit(1);
        break;
    }
}

static void do_command(int fd, struct description *d) {
    char *buff = gc_malloc(1024);
    char *buffptr = buff;
    while(conf_look != '\n') {
        *buffptr = conf_look;
        buffptr++;
        get_char(fd);
    }
    *buffptr = 0;

    d->command = buff;
}

static int do_blank(int fd) {
    if(conf_look == ' ' || conf_look == '\t') {
        printf("Matching blank line.\n");
        consume_spaces(fd);
        if(conf_look != '\n') {
            fprintf(stderr, "Line: %d Col: %d Expected end of line.\n",
                    line, column);
            exit(1);
        }
        return 1;
    }
    else if(conf_look == '\n') {
        return 1;
    }
    return 0;
}

static char *read_to_eol(int fd) {
    char buffer[2048];
    int count = 0;
    while(conf_look != '\n') {
        buffer[count++] = conf_look;
        get_char(fd);
    }
    buffer[count] = 0;
    char *ret = gc_malloc(count);
    strcpy(ret, buffer);
    return ret;
}

static int do_irc(int fd, struct config *c) {
    if(conf_look == 'i') {
        match_string(fd, "irc");
        switch(conf_look) {
        case 'h':
            match_string(fd, "host");
            consume_spaces(fd);
            c->irc_host = read_to_eol(fd);
            return 1;
            break;
        case 'p':
            match_string(fd, "port");
            char * ircport = read_to_eol(fd);
            c->irc_port = (unsigned short)strtol(ircport, NULL, 10);
            return 1;
            break;
        default:
            fprintf(stderr, "Line: %d Col: %d Expected (irchost|ircport).\n", line, column);
            exit(1);
        }
    }
    return 0;
}

static int do_local(int fd, struct config *c) {
    if(conf_look == 'l') {
        match_string(fd, "local");
        switch(conf_look) {
        case 'h':
            match_string(fd, "host");
            consume_spaces(fd);
            c->local_host = read_to_eol(fd);
            return 1;
            break;
        case 'p':
            match_string(fd, "port");
            char * localport = read_to_eol(fd);
            c->local_port = (unsigned short)strtol(localport, NULL, 10);
            return 1;
            break;
        default:
            fprintf(stderr, "Line: %d Col: %d Expected (localhost|localport).\n", line, column);
            exit(1);
        }
    }
    return 0;
}

static int do_handler(int fd, struct config *c) {
    if(conf_look == 'h') {
        struct description *desc = gc_malloc(sizeof (struct description));
        desc->next = NULL;

        match_string(fd, "handler");
        consume_spaces(fd);

        do_htype(fd, desc);
        consume_spaces(fd);

        do_itype(fd, desc);
        consume_spaces(fd);

        do_command(fd, desc);

        if(!c->descriptions) {
            c->descriptions = desc;
            c->last = desc;
        }
        else {
            c->last->next = desc;
            c->last = desc;
        }
        return 1;
    }
    return 0;
}

static char *match_single(int fd, char *token) {
    if(conf_look == token[0]) {
        match_string(fd, token);
        consume_spaces(fd);
        return read_to_eol(fd);
    }
    return NULL;
}

static void c_parse(int fd, struct config *c) {
    do {
        if (conf_look == 0) {
            get_char(fd);
        }

        char * matched;

        if(do_blank(fd)) {
            fprintf(stderr, "Doing blank line.\n");
        }
        else if(do_irc(fd, c)) {
            fprintf(stderr, "Parsing IRC conf line. %s:%d\n", c->irc_host, c->irc_port);
        }
        else if(do_local(fd, c)) {
            fprintf(stderr, "Parsing local conf line: %s:%d\n", c->local_host, c->local_port);
        }
        else if((matched = match_single(fd, "pass"))) {
            c->pass = matched;
            fprintf(stderr, "Parsing password [REDACTED]\n");
        }
        else if((matched = match_single(fd, "nick"))) {
            c->nick = matched;
            fprintf(stderr, "Parsing nick %s\n", matched);
        }
        else if((matched = match_single(fd, "user"))) {
            c->user = matched;
            fprintf(stderr, "Parsing user %s\n", matched);
        }
        else if((matched = match_single(fd, "realname"))) {
            c->real_name = matched;
            fprintf(stderr, "Parsing real name %s\n", matched);
        }
        else if((matched = match_single(fd, "channel"))) {
            struct channel *chan = gc_malloc(sizeof (struct channel));
            chan->channel_name = matched;
            chan->next = c->channels;
            c->channels = chan;
            fprintf(stderr, "Parsing channel %s\n", matched);
        }
        else if(do_handler(fd, c)) {
            fprintf(stderr, "Parsing %s %s handler line %s\n",
                    str_from_htype(c->last->h_type),
                    str_from_itype(c->last->i_type),
                    c->last->command);
        }

        match_EOF(fd, '\n');


    } while( conf_look != EOF );
}

struct config *parse_config(char * config_file) {

    int cfile = open(config_file, O_RDONLY);
    struct config *c = gc_malloc(sizeof (struct config));
    c->irc_host = NULL;
    c->local_host = NULL;
    c->irc_port = -1;
    c->local_port = -1;
    c->channels = NULL;
    c->descriptions = NULL;
    c->last = NULL;
    c_parse(cfile, c);
    return c;
}

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <signal.h>
#include <kgc.h>
#include "ircparser.h"
#include "irchandler.h"
#include "connection.h"

extern char **environ;

struct handler {
    enum handler_type h_type;
    enum input_type i_type;
    const char * command;
    char * const * argv;
    struct handler *next;

    // Stuff for persistent handlers
    pid_t pid;
    int fdin;    // Read from this.
    int fdout;   // Write to this.
    int deathcount;
};

struct handler *handlers;
struct handler *last;

static void instantiate_handler(struct handler *h,
                                enum handler_type h_type,
                                enum input_type i_type,
                                const char * command,
                                char * const argv[]) {

    h->h_type = h_type;
    h->i_type = i_type;
    h->command = command;
    h->argv = argv;
    h->next = NULL;

    h->pid = 0;
    h->fdin = -1;
    h->fdout = -1;
    h->deathcount = 0;
}

static void remove_handler(struct handler *h) {
    fprintf(stderr, "Removing handler %s.\n", h->command);
    if(handlers == h) {
        handlers = h->next;
        if(last == h)  {
            last = NULL;
        }
        return;
    }

    struct handler *current = handlers;
    while(current->next) {
        //fprintf(stderr, "Checking: %s\n", current->command);
        if(current->next == h) {
            //fprintf(stderr, "Found it!\n");
            if(last == current->next) {
                last = current;
            }
            current->next = current->next->next;
            return;
        }
        //fprintf(stderr, "Next is: %p\n", current->next);
        current = current->next;
    }
}

static int launch_child(struct handler *h) {
    // See if we already have a pid.
    if(h->deathcount >= 20) {
        printf("Handler %s marked as bad.\n", h->command);
        return 0;
    }
    if(h->pid != 0) {
        int status;

        // Check status of child process.
        int wait_result = waitpid(h->pid, &status, WNOHANG);
        if(!wait_result || !(WIFEXITED(status) || WIFSIGNALED(status))) {
            // If child has not exited, return immediately.
            // We don't want to start another.
            return 1;
        }
        else {
            fprintf(stderr, "Found dead handler: %s\n", h->command);
            h->deathcount++;
            close(h->fdin);
            close(h->fdout);
        }
        h->pid = 0;
        h->fdin = -1;
        h->fdout = -1;
    }

    int parent_in[2];
    int parent_out[2];

    pipe(parent_in);
    pipe(parent_out);

    pid_t pid = fork();
    if(pid == -1) {
        fprintf(stderr, "Failed to fork: %s\n", h->command);
        exit(1);
    }
    else if(pid) {
        //parent
        h->pid = pid;

        // Constant messages from transients are annoying.
        if(h->h_type == PERSISTENT) {
            fprintf(stderr, "Started child process: %d\n", pid);
        }

        close(parent_in[1]);
        close(parent_out[0]);
        h->fdin = parent_in[0];
        h->fdout = parent_out[1];

    }
    else {
        //child
        gc_exit();

        close(parent_in[0]);
        close(parent_out[1]);

        dup2(parent_in[1], STDOUT_FILENO);
        dup2(parent_out[0], STDIN_FILENO);
        char *env[1];
        env[0] = NULL;
        char *arg[1];
        arg[0] = NULL;
        execve(h->command, arg, environ);
        perror("Failed to launch handler");
        exit(1);
    }
    return 1;
}

void add_handler(enum handler_type h_type,
                 enum input_type i_type,
                 const char * command,
                 char * const argv[]) {

    struct handler *h = gc_malloc(sizeof (struct handler));
    instantiate_handler(h, h_type, i_type, command, argv);

    if(handlers) {
        last->next = h;
        last = h;
    }
    else {
        handlers = h;
        last = h;
    }
}

void ensure_handlers_started() {

    struct handler *h = handlers;
    while(h) {
        struct handler *next = h->next;
        if(h->h_type == PERSISTENT) {
            if(!launch_child(h)) {
                remove_handler(h);
            }
        }
        h = next;
    }
}

void add_handler_fds(fd_set *readfds, fd_set *writefds) {
    struct handler *h = handlers;
    while(h) {
        if(h->h_type == PERSISTENT) {
            FD_SET(h->fdin, readfds);
            FD_SET(h->fdout, writefds);
        }
        h = h->next;
    }
}

void transfer_to(int sock, fd_set *readfds) {
    struct handler *h = handlers;
    while(h) {
        if(h->pid != 0 && FD_ISSET(h->fdin, readfds)) {
            char buff[1024];
            while (1) {
                int readcount = read(h->fdin, buff, 1023);
                if(readcount <= 0) {
                    break;
                }
                buff[readcount] = 0;
                ensure_send_str(sock, buff, 0);

                // If we haven't filled up the buffer, break.
                // Otherwise we want to continue so we don't
                // Split up messages.
                if(readcount != 1023) {
                    break;
                }
            }
        }
        h = h->next;
    }
}

static void kill_child(struct handler *h) {
    if(h->pid > 0) {
        int status;
        // Check status of child process.
        int wait_result = waitpid(h->pid, &status, WNOHANG);
        if(wait_result && (WIFEXITED(status) || WIFSIGNALED(status))) {
            // If transient handler has exited, we're okay to continue.
        }
        else {
            // Otherwise, kill the handler.
            fprintf(stderr, "Transient child %s has not yet exited. Going to kill.\n", h->command);
            kill(h->pid, SIGKILL);
            close(h->fdin);
            close(h->fdout);
        }

        h->pid = 0;
        h->fdin = -1;
        h->fdout = -1;
    }
}

int ensure_write(int fd, char * buff, ssize_t len) {
    ssize_t count = 0;
    while(count < len) {
        ssize_t newcount = write(fd, &buff[count], len - count);
        if(newcount < 0) {
            perror("Failed to write\n"); 
            return 0;
        }
        count += newcount;
    }
    return 1;
}

void broadcast(struct message *m, fd_set *writefds) {
    struct handler *h = handlers;
    while(h) {
        if(h->h_type == PERSISTENT) {
            char *buff;
            switch(h->i_type) {
            case JSON:
                buff = message_to_json(m);
                if(! (ensure_write(h->fdout, buff, strlen(buff))
                      && ensure_write(h->fdout, "\n", 1))) {
                    fprintf(stderr, "Failed to write to handler %s\n", h->command);
                }
                break;

            case RAW:
                buff = message_to_IRC(m);
                break;
            }
        }
        else if(h->h_type == TRANSIENT) {
            kill_child(h);
            launch_child(h);
            char *buff;
            switch(h->i_type) {
            case JSON:
                buff = message_to_json(m);
                if(! (ensure_write(h->fdout, buff, strlen(buff))
                      && ensure_write(h->fdout, "\n", 1))) {
                    fprintf(stderr, "Failed to write to handler %s\n", h->command);
                }
                close(h->fdout);
                break;
            case RAW:
                buff = message_to_IRC(m);
                if( ! ensure_write(h->fdout, buff, strlen(buff))) {
                    fprintf(stderr, "Failed to write to handler %s\n", h->command);
                }
                close(h->fdout);
                break;
            }
        }
        h = h->next;
    }
}

char *str_from_htype(enum handler_type h_type) {
    switch(h_type) {
    case PERSISTENT:
        return "PERSISTENT";
        break;
    case TRANSIENT:
        return "TRANSIENT";
        break;
    }
    return NULL;
}
char *str_from_itype(enum input_type i_type) {
    switch(i_type) {
    case JSON:
        return "JSON";
        break;
    case RAW:
        return "RAW";
        break;
    }
    return NULL;
}

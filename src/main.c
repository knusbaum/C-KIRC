#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>
#include <kgc.h>
#include "connection.h"
#include "ircparser.h"
#include "irctools.h"
#include "irchandler.h"
#include "configparser.h"

int handle_ping(int sock, struct message *m);
int hello(int sock, struct message *m);
void just_dump(int sock);

int main(void) {
    GC_INIT;

    struct config *conf = parse_config("kirc.conf");

    if(conf->local_host == NULL || conf->local_port ==  (short)-1) {
        fprintf(stderr, "Local host not configured. Please fix conf.\n");
    }

    int irc_sock = launch_irc_socket(conf->local_host, conf->local_port); //STDIN_FILENO;
    int out_sock = irc_sock; //STDOUT_FILENO;

    fprintf(stderr, "IRC SOCK: %d\n", irc_sock);

    if(irc_sock < 0) {
        fprintf(stderr, "Failed to connect to IRC Client.\n");
        exit(1);
    }

    struct description *desc = conf->descriptions;
    while(desc) {

        add_handler(desc->h_type,
                    desc->i_type,
                    desc->command,
                    NULL);
        desc = desc->next;
    }

    do {
        ensure_handlers_started();

        fd_set readfds;
        fd_set writefds;
        FD_ZERO(&readfds);
        FD_ZERO(&writefds);
        FD_SET(irc_sock, &readfds);
        add_handler_fds(&readfds, &writefds);

        if(select(FD_SETSIZE, &readfds, NULL, NULL, NULL) == -1) {
            perror("Select failed!");
        }

        transfer_to(irc_sock, &readfds);

        if(FD_ISSET(irc_sock, &readfds)) {

            struct message *m = parse(irc_sock);
            char * json = message_to_json(m);
            printf("%s\n", json);

            if(handle_ping(out_sock, m)) {
                continue;
            }

            broadcast(m, &writefds);
        }

    } while(1);
    gc_exit();
}

int handle_ping(int sock, struct message *m) {
    if(strcmp(m->command, "PING") == 0) {
        char * p = pong(m);
        ensure_send_str(sock, p, 0);
        return 1;
    }
    return 0;
}

int hello(int sock, struct message *m) {
    if(strcmp(m->command, "PRIVMSG") == 0) {
        if(strcmp(m->trailing, "hello j") == 0) {
            char message[512];
            sprintf(message, "hello, %s", m->prefix.server_nick);
            char * response = respond(m, message);
            ensure_send_str(sock, response, 0);
            return 1;
        }
    }
    return 0;
}


void just_dump(int sock) {
    do {
        char buff[1024];
        buff[1023] = 0;
        ssize_t count = recv(sock, buff, 1023, 0);
        if(count <= 0) {
            fprintf(stderr, "Failed to read from socket\n");
            exit(1);
        }
        buff[count] = 0;
        printf("%s", buff);
    } while(1);
}

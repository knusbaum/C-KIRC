#ifndef IRCHANDLER_H
#define IRCHANDLER_H

#include <unistd.h>
#include <sys/select.h>
#include "ircparser.h"

enum handler_type {
    PERSISTENT,
    TRANSIENT
};

enum input_type {
    JSON,
    RAW
};

void add_handler(enum handler_type h_type,
                 enum input_type i_type,
                 const char * command,
                 char * const argv[]);

void ensure_handlers_started();

void add_handler_fds(fd_set *readfds, fd_set *writefds);

void broadcast(struct message *m, fd_set *writefds);
void transfer_to(int sock, fd_set *readfds);

char *str_from_htype(enum handler_type h_type);
char *str_from_itype(enum input_type i_type);

#endif

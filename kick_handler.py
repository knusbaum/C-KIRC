#!/usr/bin/env python2

import sys
import json

while True:
    line = sys.stdin.readline()
    
    try:
        m = json.loads(line)
        if m["command"] == "KICK" and len(m["params"]) >= 2 and m["params"][1] == "knusbaum_bot2":
            print "JOIN " + m["params"][0] + "\r\n"
            sys.stdout.flush()

    except ValueError:
        with open("pyerror.txt", "a") as myfile:
            myfile.write("Failed to parse json: [" + line + "]\n")
        sys.exit(1)

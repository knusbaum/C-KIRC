#!/usr/bin/env python2

import sys
import json

def respond(message):
    if m["command"] == "PRIVMSG":
        if m["params"][0].startswith("#"):
            return "PRIVMSG " + m["params"][0] + " :Hello, " + m["prefix"]["server_nick"] + "\r\n"
        else:
            return "PRIVMSG " + m["prefix"]["server_nick"] + " :Hello, " + m["prefix"]["server_nick"] + "\r\n"
    return ""

while True:
    line = sys.stdin.readline()

    try:
        m = json.loads(line)

        if m["command"] == "PRIVMSG" and m["trailing"].lower().startswith("hello"):
            print respond(m)
            sys.stdout.flush()

    except ValueError:
        with open("pyerror.txt", "a") as myfile:
            myfile.write("Failed to parse json: [" + line + "]")
        sys.exit(1)

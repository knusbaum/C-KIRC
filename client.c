#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <kgc.h>
#include "src/connection.h"
#include "src/configparser.h"

void tabulate(char *buff, ssize_t len) {
    for(int i = 0; i < len; i++) {
        if(buff[i] == '\r') {
            buff[i] = '\n';
            buff[i+1] = '\t';
        }
    }
}

int main(void) {
    GC_INIT;

    struct config *conf = parse_config("kirc.conf");

    if(conf->irc_host == NULL || conf->irc_port == (short)-1) {
        fprintf(stderr, "Remote host not configured. Please fix conf.\n");
    }
    else if(conf->local_host == NULL || conf->local_port ==  (short)-1) {
        fprintf(stderr, "Local host not configured. Please fix conf.\n");
    }

    printf("Going to connect: %s:%d\n", conf->irc_host, conf->irc_port);
    int irc_sock = launch_irc_socket(conf->irc_host, conf->irc_port);
    int out_sock = irc_sock;

    if(conf->pass) {
        char passbuff[1024];
        sprintf(passbuff, "PASS %s\r\n", conf->pass);
        ensure_send_str(irc_sock, passbuff, 0);
        printf("<= PASS [REDACTED]\n");
    }

    if(conf->nick) {
        char nickbuff[1024];
        sprintf(nickbuff, "NICK %s\r\n", conf->nick);
        ensure_send_str(irc_sock, nickbuff, 0);
    }
    if(conf->user) {
        char * realname = "nobody";
        if(conf->real_name) {
            realname = conf->real_name;
        }

        char userbuff[1024];
        sprintf(userbuff, "USER %s 0 * :%s\r\n", conf->user, conf->real_name);
        ensure_send_str(irc_sock, userbuff, 0);
    }

    struct channel *chan = conf->channels;
    while(chan) {
        char chanbuff[1024];
        sprintf(chanbuff, "JOIN %s\r\n", chan->channel_name);
        ensure_send_str(out_sock, chanbuff, 0);
        chan = chan->next;
    }

    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock == -1) {
        perror("Failed to get socket");
        return -1;
    }

    struct sockaddr_in addr;


    printf("Going to listen: %s:%d\n", conf->local_host, conf->local_port);
    addr.sin_family = AF_INET;
    addr.sin_port = htons(conf->local_port);
    inet_aton(conf->local_host, (struct in_addr *)&addr.sin_addr.s_addr);

    int bind_result = bind(sock, (struct sockaddr*)&addr, sizeof(addr));
    if(bind_result == -1) {
        perror("Failed to bind socket");
        return -1;
    }

    int listen_result = listen(sock, 2);
    if(listen_result == -1) {
        perror("Failed to listen on socket");
        return -1;
    }

    while (1) {

        int remotefd;
        while(1) {

            fd_set readfds;

            FD_ZERO(&readfds);

            FD_SET(sock, &readfds);
            FD_SET(irc_sock, &readfds);
            FD_SET(STDIN_FILENO, &readfds);

            select(FD_SETSIZE, &readfds, NULL, NULL, NULL);

            if( FD_ISSET(STDIN_FILENO, &readfds) ) {
                printf("Sending message to IRC.\n");
                transfer(STDIN_FILENO, irc_sock);
            }

            if( FD_ISSET(irc_sock, &readfds) ) {
                char buff[1024];
                ssize_t count = recv(irc_sock, buff, 1023, 0);
                if(count <= 0) {
                    fprintf(stderr, "Failed to send to IRC server\n");
                    exit(1);
                }
                buff[count] = 0;
                char * pingspot = strstr(buff, "PING");
                if(pingspot != NULL) {
                    char ping[512];
                    char response[512];
                    sscanf(pingspot, "PING :%99[^\r^\n]", ping);
                    printf("=>\t%s\n", buff);
                    sprintf(response, "PONG %s\r\n", ping);
                    printf("<=\t%s\n", response);
                    quiet_ensure_send_str(irc_sock, response, 0);
                }
                else {
                    tabulate(buff, count);
                    printf("Drop:\t%s\n", buff);
                }
            }

            if( FD_ISSET(sock, &readfds) ) {
                remotefd = accept(sock, NULL, NULL);
                printf("Got local connection: %d\n", remotefd);
                break;
            }
        }

        while (1) {
            fd_set readfds;
            FD_ZERO(&readfds);

            FD_SET(remotefd, &readfds);
            FD_SET(irc_sock, &readfds);
            FD_SET(STDIN_FILENO, &readfds);

            select(FD_SETSIZE, &readfds, NULL, NULL, NULL);

            if( FD_ISSET(STDIN_FILENO, &readfds) ) {
                printf("Sending message to IRC.\n");
                transfer(STDIN_FILENO, irc_sock);
            }

            if( FD_ISSET(irc_sock, &readfds) ) {
                char buff[1024];
                ssize_t count = recv(irc_sock, buff, 1023, 0);
                if(count <= 0) {
                    fprintf(stderr, "Failed to send to IRC server\n");
                    exit(1);
                }
                buff[count] = 0;
                ensure_send(remotefd, buff, count, 0);
                tabulate(buff, count);
                printf("=>\t%s\n", buff);
            }

            if( FD_ISSET(remotefd, &readfds) ) {
                char buff[1024];
                ssize_t count = recv(remotefd, buff, 1023, 0);

                if(count <= 0) {
                    close(remotefd);
                    fprintf(stderr, "Failed to send to remote client\n");
                    break;
                }

                buff[count] = 0;
                ensure_send(irc_sock, buff, count, 0);
                tabulate(buff, count);
                printf("<=\t%s\n", buff);
            }

        }

    }

    gc_exit();
}

#!/usr/bin/env python2

import sys
import json
import re
import datetime

def respond(message, response):
    if m["command"] == "PRIVMSG":
        if m["params"][0].startswith("#"):
            return "PRIVMSG " + m["params"][0] + " :" + response + "\r\n"
        else:
            return "PRIVMSG " + m["prefix"]["server_nick"] + " :" + response + "\r\n"
    return ""


while True:
    line = sys.stdin.readline()

    try:
        m = json.loads(line)
        if m["command"] == "433":
            sys.stderr.write("Nickname is in use. Exiting.\n")
            sys.stderr.flush()
            print "QUIT\r\n"
            sys.stdout.flush()

    except ValueError:
        with open("pyerror.txt", "a") as myfile:
            myfile.write("Failed to parse json: [" + line + "]\n")
        sys.exit(1)
